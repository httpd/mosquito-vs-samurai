//
//  FingerSwipe.h
//  AppScaffold
//
//  Created by Piotrek Knapczyk on 31.12.2011.
//  Copyright (c) 2011 IdeaStudio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GLCommon.h"

#define NUM_VERTICES 10

@interface FingerSwipe : SPDisplayObject <SPAnimatable> {
    BOOL complete;
@private
    double idleTime_;
    
    NSMutableArray *touchPoints_;
    int numTouchPoints_;   // an optimization for [touchPoints_ count]
    int numIndicesNeeded_; // an optimization for our render function. It's 3 * num triangles we are rendering.
    
    ColoredVertexData vertices_[NUM_VERTICES];
    
    int stride_;
}

- (void)clear;
- (void)addTouchPointX:(float)x Y:(float)y;

@property BOOL complete;
@end