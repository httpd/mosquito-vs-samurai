//
//  SamuraiClass.h
//  AppScaffold
//
//  Created by Piotrek Knapczyk on 31.12.2011.
//  Copyright (c) 2011 IdeaStudio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyVector.h"

extern int GameSpeed;

@interface SamuraiClass : SPDisplayObjectContainer
{
    int width;
    int height;
    int vy;
    int vx;
    SPQuad *katana;
    double vectorRad;
    MyVector *speedVector;
    SPImage *samuraiImage;
    int actualTexture;
}
- (void)runAnimation: (NSString *)animation;
- (id)initAndShow;
- (void)onTouch:(SPTouchEvent*)event;
- (void)jump;

- (void)onEnterFrame: (SPEnterFrameEvent *)event;

- (void)addTouchPoint: (SPPoint *)touchPoint;
- (void)clearTouchPointArray;
- (void)changeTexture: (int)texture;
@property int vy;
@property int vx;
@property double vectorRad;
@property int speedVec;
@property (nonatomic, retain) MyVector *speedVector;
@property (strong) NSArray *touchPointArray;

@end
