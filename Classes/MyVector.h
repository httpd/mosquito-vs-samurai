//
//  MyVector.h
//  AppScaffold
//
//  Created by Piotrek Knapczyk on 05.01.2012.
//  Copyright (c) 2012 IdeaStudio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyVector : NSObject
{
    int x,y;
}
- (id)initWithAngle: (double)VectorAngle andVartible: (float)VectorVartible;
- (id)initWithX: (int)X andY: (int)Y;

+ (MyVector *)SumaWektora: (MyVector *)vector1 iWektora: (MyVector *)vector2;
- (NSString *)description;

@property double angle;
@property float vartible;
@property int x;
@property int y;

@end
