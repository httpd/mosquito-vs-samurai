//
//  MyVector.m
//  AppScaffold
//
//  Created by Piotrek Knapczyk on 05.01.2012.
//  Copyright (c) 2012 IdeaStudio. All rights reserved.
//

#import "MyVector.h"

@implementation MyVector
@synthesize x ,y;


- (double)angle;
{
    if (self.x ==0 || self.y == 0) {
        return 0;
    }
    return atan((double)self.x/self.y);
}
- (float)vartible
{
    if (x==0) {
        return y;
    }
    if (y==0) {
        return x;
    }
    return sqrt(self.y*self.y+self.x*self.x);
}

- (void)setAngle: (double)eangle
{
    double paramter = 360;
    eangle = modf(eangle, &paramter) ;
    float v = self.vartible;
    //NSLog(@"%f, %f", cos(eangle)*v, sin(eangle)*v);
    self.y=cos(eangle)*v;
    self.x=sin(eangle)*v;
//    if (eangle<0 || eangle>180) {
//        if (self.y>0) {
//            self.y=-self.y;
//        }
//        if (self.x>0) {
//            self.x=-self.x;
//        }
//    }
}

- (void)setVartible: (float)evariable
{
    if (evariable==0) {
        self.x=0;
        self.y=0;
    }else
    {
    self.y = sin(y/self.vartible)*evariable;
    self.x = cos(x/self.vartible)*evariable;
    }
}

- (id)initWithAngle: (double)VectorAngle andVartible: (float)VectorVartible
{
    if (self = [super init]) {
        self.y=cos(VectorAngle)*VectorVartible;
        self.x=sin(VectorAngle)*VectorVartible;
    }
    return self;
}
- (id)initWithX: (int)X andY: (int)Y;
{
    if (self = [super init]) {
        self.x = X;
        self.y = Y;
    }
    return self;
}
- (NSString *)description
{
    return [NSString stringWithFormat:@"x: %d, y: %d, angle: %f, vartible: %f", self.x, self.y, self.angle, self.vartible];
}

+ (MyVector *)SumaWektora: (MyVector *)vector1 iWektora: (MyVector *)vector2
{
    
    MyVector *vectorOstateczny = [[MyVector alloc] initWithX:vector1.x+vector2.x andY:vector1.y+vector2.y];
                          
    return vectorOstateczny;
}

@end
