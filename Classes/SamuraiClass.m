//
//  SamuraiClass.m
//  AppScaffold
//
//  Created by Piotrek Knapczyk on 31.12.2011.
//  Copyright (c) 2011 IdeaStudio. All rights reserved.
//

#import "SamuraiClass.h"
@interface SamuraiClass()
{
    SPTexture *s_1;
    SPTexture *s_2;
    SPTexture *s_3;
    SPTexture *s_4;
    SPTexture *s_5;

}
@end

@implementation SamuraiClass
@synthesize vy,vx, vectorRad, speedVec, speedVector;
@synthesize touchPointArray = _touchPointArray;
- (id)initAndShow
{
    if (self = [super init]) {
        self.vx = 0;
        self.vy = 0;
        width = 98;
        height = 178;
//        SPQuad *samuraiDebug = [[SPQuad alloc] initWithWidth:50 height:50 color:0x00ff00];
//        [self addChild:samuraiDebug];
        samuraiImage = [SPImage imageWithContentsOfFile:@"s_3.png"];
        actualTexture = 3;
        [self addChild:samuraiImage];
        vectorRad = SP_D2R(0);
        speedVec = 0;
        vectorRad = SP_D2R(90);
        speedVector = [[MyVector alloc] initWithAngle:SP_D2R(90) andVartible:-200];
        s_1 = [SPTexture textureWithContentsOfFile:@"s_1.png"];
        s_2 =[SPTexture textureWithContentsOfFile:@"s_2.png"];
        s_3 = [SPTexture textureWithContentsOfFile:@"s_3.png"];
        s_4 = [SPTexture textureWithContentsOfFile:@"s_4.png"];
        s_5 = [SPTexture textureWithContentsOfFile:@"s_5.png"];


    }
    return self;
}
- (void)runAnimation:(NSString *)animation
{
    if (animation == @"cios") {
        SPTween *tween = [SPTween tweenWithTarget:katana time:0.1f transition:SP_TRANSITION_LINEAR];
        [tween animateProperty:@"rotation" targetValue:SP_D2R(40)];        
        [self.stage.juggler addObject:tween];
        SPTween *tween2 = [SPTween tweenWithTarget:katana time:0.1f transition:SP_TRANSITION_LINEAR];
        [tween2 animateProperty:@"rotation" targetValue:SP_D2R(-40)];
        [tween2 setDelay:0.1f];
        [self.stage.juggler addObject:tween2];

    }if (animation == @"cios2") {
        SPTween *tween = [SPTween tweenWithTarget:katana time:1 transition:SP_TRANSITION_LINEAR];
        [tween animateProperty:@"rotation" targetValue:SP_D2R(-220)];        
        [self.stage.juggler addObject:tween];
        SPTween *tween2 = [SPTween tweenWithTarget:katana time:1 transition:SP_TRANSITION_LINEAR];
        [tween2 animateProperty:@"rotation" targetValue:SP_D2R(-40)];
        [tween2 setDelay:1.0f];
        [self.stage.juggler addObject:tween2];

    }
}
- (void)jump
{
    if (self.touchPointArray.count>=4) {
        SPPoint *touchBeg = [self.touchPointArray objectAtIndex:0];
        SPPoint *touchEnd = [self.touchPointArray objectAtIndex:3];
        
        int x = (touchEnd.x-touchBeg.x)*5;
        int y = (touchEnd.y-touchBeg.y)*5;
        
        if (x>0)
            x+=200;
        else
            x+=-200;
        if (y>0) 
            y+=200;
        else
            y+=-200;
        MyVector *vectorRuchu = [[MyVector alloc] initWithX:(touchEnd.x-touchBeg.x)*5 andY:(touchBeg.y-touchEnd.y)*5];
        self.speedVector = vectorRuchu;
    }
}
- (void)addTouchPoint: (SPPoint *)touchPoint
{
        self.touchPointArray = [self.touchPointArray arrayByAddingObject: touchPoint];
}
- (void)clearTouchPointArray
{
    self.touchPointArray = [NSArray array];
}

- (void)changeTexture: (int)texture
{
    
}
- (void)onEnterFrame:(SPEnterFrameEvent *)event
{
    MyVector *grawitacja = [[MyVector alloc] initWithX:0 andY:-800*GameSpeed*event.passedTime];
    
    
    self.speedVector = [MyVector SumaWektora:grawitacja iWektora:self.speedVector];
    
    float py = self.y-speedVector.y*event.passedTime;
    float px = self.x+speedVector.x*event.passedTime;
    
    if (px<0-self.width+200) {
        
        if (self.speedVector.x<0) {
            self.speedVector.x = -1*self.speedVector.x;
        }
    }
    if (px>320-self.width+width) {
        if (self.speedVector.x>0) {
            self.speedVector.x = -1*self.speedVector.x;
        }
    }
    if (py>480-self.height) {
        self.speedVector.y = 0;
        self.speedVector.x = self.speedVector.x*0.8;

        py = 480-self.height;
        px = self.x+speedVector.x*event.passedTime;
    }
    
    
    if ((py>=480 || speedVector.y == 0)&& actualTexture != 3) {
        samuraiImage.texture = s_3;
        actualTexture = 3;
    }else if(speedVector.y > 0 && actualTexture != 5)
    {
        actualTexture = 5;
        samuraiImage.texture = s_5;
    }else if(speedVector.y<0 && actualTexture !=4)
    {
        actualTexture = 4;
        samuraiImage.texture = s_4;
    }

    self.y = py;
    self.x = px;

}
@end
