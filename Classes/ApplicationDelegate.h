//
//  AppScaffoldAppDelegate.h
//  AppScaffold
//

#import <UIKit/UIKit.h>
#import "MenuViewController.h"

@interface ApplicationDelegate : NSObject <UIApplicationDelegate> 
{
  @private 
    UIWindow *mWindow;
    SPView *mSparrowView;
    MenuViewController *mMenu;
}

@end
