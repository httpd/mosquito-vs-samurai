//
//  MenuViewController.m
//  AppScaffold
//
//  Created by Piotrek Knapczyk on 12.02.2012.
//  Copyright (c) 2012 IdeaStudio. All rights reserved.
//

#import "MenuViewController.h"
#import "Game.h" 

@implementation MenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    [SPPoint purgePool];
    [SPMatrix purgePool];
    [SPRectangle purgePool];
    // Release any cached data, images, etc that aren't in use.
}
- (IBAction)playGame {
    mSparrowView = [[SPView alloc] initWithFrame:self.view.window.bounds]; 
    [self.view.window addSubview:mSparrowView];
    [self.view removeFromSuperview];
    @autoreleasepool {
        Game *game = [[Game alloc] init];        
        mSparrowView.stage = game;
        mSparrowView.multipleTouchEnabled = NO;
        mSparrowView.frameRate = 60.0f;
        [mSparrowView start];
    }
}
- (void)stopGame
{
    [mSparrowView stop];
}
- (void)startGame
{
    [mSparrowView start];
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)s:(id)sender {
}
@end
