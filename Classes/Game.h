//
//  Game.h
//  AppScaffold
//

#import <Foundation/Foundation.h>
#import "SamuraiClass.h"
#import "FingerSwipe.h"
#import "MosquitoClass.h"
#import <BackgroundClass.h>

@interface Game : SPStage
{
    BackgroundClass *background;
    BOOL pierwszyRuch;
    SamuraiClass* samurai;
    FingerSwipe *swipe;
    MosquitoClass *mos1;
    MosquitoClass *mos2;
    MosquitoClass *mos3;
    MosquitoClass *mos4;
    MosquitoClass *mos5;
    NSArray *touchPointArray;
    SPTextField *FPS;
}
@end
