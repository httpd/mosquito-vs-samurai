//
//  MenuViewController.h
//  AppScaffold
//
//  Created by Piotrek Knapczyk on 12.02.2012.
//  Copyright (c) 2012 IdeaStudio. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface MenuViewController : UIViewController
{
    SPView *mSparrowView;
}

- (void)stopGame;
- (void)didReceiveMemoryWarning;
- (void)startGame;
@end
