//
//  MosquitoClass.h
//  AppScaffold
//
//  Created by Piotrek Knapczyk on 31.12.2011.
//  Copyright (c) 2011 IdeaStudio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyVector.h"

@interface MosquitoClass : SPDisplayObjectContainer
{
    BOOL hit;
    MyVector *speedVector;
}

- (void)destroy;
- (BOOL)checkColisionAtPoint: (SPPoint *)touchPoint;
- (void)onEnterFrame:(SPEnterFrameEvent *)event;
@property BOOL hit;
@end
