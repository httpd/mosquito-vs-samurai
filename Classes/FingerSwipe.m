//
//  FingerSwipe.m
//  AppScaffold
//
//  Created by Piotrek Knapczyk on 31.12.2011.
//  Copyright (c) 2011 IdeaStudio. All rights reserved.
//


#import "FingerSwipe.h"

#define DEFAULT_ALPHA 0.9

@interface FingerSwipe (Private)
- (void)updateTriangles;
- (void)updateColor;
@end

// every three entries represents one triangle
// we may or may not use all of the triangles
// the values are indices into the vertices_ array
static const GLubyte triangles[24] = {
    0, 1, 2,
    1, 2, 3,
    2, 3, 4,
    3, 4, 5,
    4, 5, 6,
    5, 6, 7,
    6, 7, 8,
    7, 8, 9
};

// we use this to calculate the perpendicular vector to our sample touch points
// this gives our swipe some width on the screen
static Vector2D perp; 

@implementation FingerSwipe
@synthesize complete;
- (id)init {
    if ((self = [super init])) {
        touchPoints_ = [[NSMutableArray alloc] init];
        self.alpha = DEFAULT_ALPHA;
        stride_ = sizeof(ColoredVertexData);
        [self updateColor];
        complete = YES;
    }
    return self;
}

- (void)updateColor {
    for (int i=0; i<NUM_VERTICES; i++) {
        vertices_[i].color = ColorRGBAMake(1,1,1, self.alpha);
    }
}

- (void)clear {
    [touchPoints_ removeAllObjects];
    numTouchPoints_ = 0;
    complete = YES;
}

- (void)addTouchPointX:(float)x Y:(float)y {
    complete = NO;
    idleTime_ = 0;
    [touchPoints_ addObject:[SPPoint pointWithX:x y:y]]; // newest points are added at the end
    numTouchPoints_ = [touchPoints_ count];
    if (numTouchPoints_ > 5) { // make sure we have no more than the 5 most recent points
        [touchPoints_ removeObjectAtIndex:0]; // remove the oldest point
        numTouchPoints_--;
    }
    numIndicesNeeded_ = numTouchPoints_ * 6 - 6;
    [self updateTriangles];
}

- (void)updateTriangles {
    if (numTouchPoints_ < 2) {
        return;
    }
    
    // index into the vertices_ array
    GLubyte v = 0;
    float currX, currY, prevX, prevY;
    
    // walk the points list from the oldest to the newest
    // as we walk, generate triangles around the sample points
    SPPoint *pPrev = [touchPoints_ objectAtIndex:0]; // This is the oldest point
    prevX = pPrev.x;
    prevY = pPrev.y;
    Vertex2DSet(&vertices_[v++].vertex, prevX, prevY);
    
    // create the tail
    SPPoint *pCurr = [touchPoints_ objectAtIndex:1];
    currX = pCurr.x;
    currY = pCurr.y;
    
    Vector2DSet(&perp, prevY-currY, currX-prevX);
    Vector2DScaleToMagnitude(&perp, 0.6);
    
    // the two points around our sample point, to give the swipe its thickness
    Vertex2DSet(&vertices_[v++].vertex, currX+perp.x, currY+perp.y); // add the perp vector
    Vertex2DSet(&vertices_[v++].vertex, currX-perp.x, currY-perp.y); // subtract the perp vector
    
    // draw the body
    for (NSUInteger i = 2; i < numTouchPoints_; i++) {
        pPrev = pCurr;
        prevX = currX;
        prevY = currY;
        pCurr = [touchPoints_ objectAtIndex:i];
        currX = pCurr.x;
        currY = pCurr.y;
        
        Vector2DSet(&perp, prevY-currY, currX-prevX);  // update the perpendicular vector
        Vector2DScaleToMagnitude(&perp, 1 + i * 1.2); // increase the width as we go
        
        Vertex2DSet(&vertices_[v++].vertex, currX+perp.x, currY+perp.y); // add the perp vector
        Vertex2DSet(&vertices_[v++].vertex, currX-perp.x, currY-perp.y); // subtract the perp vector
    }
    
    // create the tip by adding an extra vertex beyond the most recent point, in the same direction we were traveling
    Vertex2DSet(&vertices_[v].vertex, currX + 0.6*(currX-prevX), currY + 0.6*(currY-prevY));
}

- (void)render:(SPRenderSupport*)support {
    if (numTouchPoints_ < 2) {
        return;
    }
    
    [support bindTexture:nil];
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glVertexPointer(2, GL_FLOAT, stride_, &vertices_[0].vertex);
    glColorPointer(4, GL_FLOAT, stride_, &vertices_[0].color);
    glDrawElements(GL_TRIANGLES, numIndicesNeeded_, GL_UNSIGNED_BYTE, triangles);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
}

- (SPRectangle *)boundsInSpace:(SPDisplayObject *)targetCoordinateSpace {
    return [SPRectangle rectangleWithX:0 y:0 width:0 height:0];
}

- (void)advanceTime:(double)seconds {
    if (numTouchPoints_ < 2) {
        return;
    }
    idleTime_ += seconds;
    float a = DEFAULT_ALPHA - MIN(idleTime_ / 0.2, DEFAULT_ALPHA); // fade it out over 0.2 seconds
    self.alpha = a;
    [self updateColor];
    if (a == 0) {
        [self clear];
    }
}

- (BOOL)isComplete {
    return NO;
}



@end