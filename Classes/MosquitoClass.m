//
//  MosquitoClass.m
//  AppScaffold
//
//  Created by Piotrek Knapczyk on 31.12.2011.
//  Copyright (c) 2011 IdeaStudio. All rights reserved.
//

#import "MosquitoClass.h"

@implementation MosquitoClass
@synthesize hit;
- (id)init
{
    if (self = [super init]) {
        SPQuad *debugImage = [SPQuad quadWithWidth:10 height:10 color:0xff0000];
        [self addChild:debugImage];
        self.pivotX = self.width/2;
        self.pivotY = self.width/2;
        speedVector = [[MyVector alloc] initWithAngle:SP_D2R(-91) andVartible:200];

    }
    return self;
}

- (void)destroy
{
}
- (void)onEnterFrame:(SPEnterFrameEvent *)event
{
    MyVector *grawitacja = [[MyVector alloc] initWithX:0 andY:-200*event.passedTime];
    
    
    speedVector = [MyVector SumaWektora:grawitacja iWektora:speedVector];
    
    float py = self.y-speedVector.y*event.passedTime;
    float px = self.x+speedVector.x*event.passedTime;

    
    if (px<-50) {
        if (speedVector.x<0) {
            speedVector = [[MyVector alloc] initWithAngle:SP_D2R(80+arc4random()%20) andVartible:600];
        }
    }
    if (px>380-self.width) {
        if (speedVector.x>0) {
            speedVector = [[MyVector alloc] initWithAngle:SP_D2R(-1*(80+arc4random()%20)) andVartible:600];

        }
    }
    if (py>480-self.height) {
        speedVector.y = -1*speedVector.y;
    }   
    if (py<-30) {
        speedVector.y = -1*speedVector.y;
    }
    self.y = py;
    self.x = px;
    
}

- (BOOL)checkColisionAtPoint: (SPPoint *)touchPoint
{
    SPRectangle *bounds1 = [SPRectangle rectangleWithX:self.x y:self.y width:self.width*2 height:self.height*2];
    SPRectangle *bounds2 = [SPRectangle rectangleWithX:touchPoint.x y:touchPoint.y width:1 height:1];
    if ([bounds1 intersectsRectangle:bounds2]) {
        return YES;
    }
    return NO;
}
@end
