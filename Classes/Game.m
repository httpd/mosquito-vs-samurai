//
//  Game.m
//  AppScaffold
//

#import "Game.h" 
int GameSpeed = 2;

@implementation Game

- (id)initWithWidth:(float)width height:(float)height
{
    if ((self = [super initWithWidth:width height:height]))
    {
        background = [[BackgroundClass alloc] init];
        [self addChild:background];
        samurai = [[SamuraiClass alloc] initAndShow];
        samurai.x = width/2-samurai.width/2;
        //samurai.y = self.height-60;
        samurai.y = 10;
        [self addChild:samurai];
        [self addEventListener:@selector(onEnterFrame:) atObject:self forType:SP_EVENT_TYPE_ENTER_FRAME];
        [self addEventListener:@selector(onTouch:) atObject:self forType:SP_EVENT_TYPE_TOUCH];
        swipe = [[FingerSwipe alloc] init];
        [self addChild:swipe];
        mos1 = [[MosquitoClass alloc] init];
        mos1.x = 80;
        mos1.y = 100;
        [self addChild:mos1];
        pierwszyRuch = YES;
        touchPointArray = [[NSArray alloc] init];
        FPS = [[SPTextField alloc] initWithText:@"0.0 fps"];
        [self addChild:FPS];

    }
    return self;
}
- (void)onEnterFrame:(SPEnterFrameEvent *)event
{
    //NSLog(@"FrameRate: %f FPS", 1/event.passedTime);
    //[enemy moveBy:event.passedTime * enemy.velocity];
    FPS.text = [NSString stringWithFormat:@"%f fps", 1/event.passedTime];
    [samurai onEnterFrame:event];
    [mos1 onEnterFrame:event];
    if (samurai.y==480-samurai.height) {
        pierwszyRuch=YES;
    }

}
-(void)onTouch:(SPTouchEvent*)event
{
    SPTouch *touchBegin = [[event touchesWithTarget:self andPhase:SPTouchPhaseBegan] anyObject];
    if (touchBegin)
    {
        SPPoint *touchPosition = [touchBegin locationInSpace:self];
        [swipe clear];
        //[samurai jump:event];
    }
    touchBegin = [[event touchesWithTarget:self andPhase:SPTouchPhaseMoved] anyObject];
    if (touchBegin)
    {
        SPPoint *touchPosition = [touchBegin locationInSpace:self];
        if (pierwszyRuch && touchPointArray.count<=4) {
            touchPointArray = [touchPointArray arrayByAddingObject: touchPosition];
        }else if (touchPointArray.count>4 && pierwszyRuch) {
            samurai.touchPointArray = touchPointArray;
            [samurai jump];
            [samurai clearTouchPointArray];
            pierwszyRuch = false;
            touchPointArray = [NSArray array];
        }
        [swipe addTouchPointX:touchPosition.x Y:touchPosition.y];
        if ([mos1 checkColisionAtPoint:touchPosition]) {
            [self removeChild:mos1];
        }
        //[samurai jump:event];
    }
    touchBegin = [[event touchesWithTarget:self andPhase:SPTouchPhaseEnded] anyObject];
    if (touchBegin)
    {
        [swipe clear];
        //[samurai jump:event];
    }
    touchBegin = [[event touchesWithTarget:self andPhase:SPTouchPhaseCancelled] anyObject];
    if (touchBegin)
    {
        SPPoint *touchPosition = [touchBegin locationInSpace:self];
        [swipe clear];
        //[samurai jump:event];
    }

    
}

@end
