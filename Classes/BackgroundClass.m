//
//  BackgroundClass.m
//  AppScaffold
//
//  Created by Piotrek Knapczyk on 31.12.2011.
//  Copyright (c) 2011 IdeaStudio. All rights reserved.
//

#import "BackgroundClass.h"

@implementation BackgroundClass
- (id)init
{
    if (self = [super init]) {
        SPImage *image = [[SPImage alloc] initWithContentsOfFile:@"background.png"];
        [self addChild:image];
    }
    return self;
}

@end
