//
//  AppScaffoldAppDelegate.m
//  AppScaffold
//

#import "ApplicationDelegate.h"
#import "Game.h" 
@implementation ApplicationDelegate

- (id)init
{
    if ((self = [super init]))
    {
        mWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        mMenu = [[MenuViewController alloc] init];
        [mWindow addSubview:mMenu.view];
    }
    return self;
}

- (void)applicationDidFinishLaunching:(UIApplication *)application 
{    
    @autoreleasepool {
    [SPStage setSupportHighResolutions:YES];
    [SPAudioEngine start];
        
    [mWindow makeKeyAndVisible];
    
    }
}

- (void)applicationWillResignActive:(UIApplication *)application 
{    
    [mMenu stopGame];
}

- (void)applicationDidBecomeActive:(UIApplication *)application 
{
	[mMenu startGame];
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
    [mMenu didReceiveMemoryWarning];
}

- (void)dealloc 
{
    [SPAudioEngine stop];
}

@end
