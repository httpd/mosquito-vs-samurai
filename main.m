//
//  main.m
//  AppScaffold
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    @autoreleasepool {
        int retVal = UIApplicationMain(argc, argv, nil, @"ApplicationDelegate");
        return retVal;
    }
}
